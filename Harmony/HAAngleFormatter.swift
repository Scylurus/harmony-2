//
//  HAAngleFormatter.swift
//  Harmony
//
//  Created by Сергей Цыба on 14.04.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import Foundation
import Ascension


class HAAngleFormatter {
    enum Style {
        case standard
        case radian
        case decimalDegree
        case sexagesimalDegree
        case decimalHour
        case sexagesimalHour
    }
    
    var style: Style = .standard
    
    var fractionDigits = 6 {
        didSet {
            self.fractionFormatter.minimumFractionDigits = self.fractionDigits
            self.fractionFormatter.maximumIntegerDigits = self.fractionDigits
        }
    }
    
    private lazy var fractionFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumIntegerDigits = 0
        formatter.minimumFractionDigits = self.fractionDigits
        formatter.maximumFractionDigits = self.fractionDigits
        return formatter
    }()
    
    /// Formats the specified angular value.
    public func string(from value: ASAngle) -> String {
        switch self.style {
        case .standard, .radian:
            let (integer, fraction) = modf(value)
            
            let fractionNumber = NSNumber(value: fabs(fraction))
            let formattedMantissa = self.fractionFormatter.string(from: fractionNumber)!
            return "\(Int(integer))\(formattedMantissa)"
            
        case .decimalDegree:
            // value is split and rounded manually in order to account for future gradal
            // normalization in case fraction rounds to a unit
            let (degrees, fraction) = decimalParts(of: value * 180.0 / .pi, roundedTo: self.fractionDigits)
            
            let fractionNumber = NSNumber(value: fraction)
            let formattedFraction = self.fractionFormatter.string(from: fractionNumber)!
            return "\(degrees)°\(formattedFraction)"
            
        case .sexagesimalDegree:
            let (degrees, minutes, seconds) = roundedSexagesimalParts(of: value * 180.0 / .pi)
            
            let formattedMinutes = minutes < 10 ? "0\(minutes)" : "\(minutes)"
            let formattedSeconds = seconds < 10 ? "0\(seconds)" : "\(seconds)"
            return "\(degrees)°\(formattedMinutes)′\(formattedSeconds)″"
            
        case .decimalHour:
            // see comment on decimal degree case
            let (hours, fraction) = decimalParts(of: value * 12.0 / .pi, roundedTo: self.fractionDigits)
            
            let fractionNumber = NSNumber(value: fraction)
            let formattedFraction = self.fractionFormatter.string(from: fractionNumber)!
            return "\(hours)ʰ\(formattedFraction)"
            
        case .sexagesimalHour:
            let (hours, minutes, seconds) = roundedSexagesimalParts(of: value * 12.0 / .pi)
            
            let formattedMinutes = minutes < 10 ? "0\(minutes)" : "\(minutes)"
            let formattedSeconds = seconds < 10 ? "0\(seconds)" : "\(seconds)"
            return "\(hours)ʰ\(formattedMinutes)ᵐ\(formattedSeconds)ˢ"
        }
    }
}


// MARK: - Convenience functionality

/// Returns integer and fractional parts of the specified value with fractional part rounded to
/// the specified digit.
private func decimalParts(of value: Double, roundedTo fractionDigits: Int) -> (integer: Int, fraction: Double) {
    var (integer, fraction) = modf(value)
    let exponent = pow(10.0, Double(fractionDigits))
    var mantissa = round(fabs(fraction) * exponent)
    
    if mantissa == exponent * 10.0 {
        integer += 1
        mantissa = 0.0
    }
    
    return (Int(integer), mantissa / exponent)
}

/// Returns components of the sexagesimal representation rounded to the nearest second for the
/// specified decimal angular value.
func roundedSexagesimalParts(of value: Double) -> (integer: Int, minutes: Int, seconds: Int) {
    var (integer, fraction) = modf(value)
    var (minutes, minuteFraction) = modf(fabs(fraction * 60.0))
    var seconds = round(minuteFraction * 60.0)
    
    if seconds == 60.0 {
        seconds = 0.0
        minutes += 1.0
    }
    
    if minutes == 60.0 {
        minutes = 0.0
        integer += 1.0
    }
    
    return (Int(integer), Int(minutes), Int(seconds))
}
