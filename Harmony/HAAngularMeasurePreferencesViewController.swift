//
//  HAAngularMeasurePreferencesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 06.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit


class HAAngularMeasurePreferencesViewController: UITableViewController {
    fileprivate let styles: [[HAAngleFormatter.Style]] = [
        [
            .standard
        ],
        [
            .radian,
            .decimalDegree,
            .sexagesimalDegree,
            .decimalHour,
            .sexagesimalHour
        ]
    ]
    
    public init() {
        super.init(style: .grouped)
        self.title = NSLocalizedString("Angular measure", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - View lifecycle

extension HAAngularMeasurePreferencesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.registerNibForCellReuse(named: "PreferenceCell")
    }
}


// MARK: - Table view delegate

extension HAAngularMeasurePreferencesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        UserDefaults.standard.angleFormatStyle = self.styles[indexPath.section][indexPath.row]
        
        // NOTE: reloadSections does not cancel row deselection animation
        // unlike reloadData
        let sectionsRange = NSMakeRange(0, 2)
        let sections = IndexSet(integersIn: sectionsRange.toRange() ?? 0..<0)
        tableView.reloadSections(sections, with: .automatic)
    }
}


// MARK: - Table view data source

extension HAAngularMeasurePreferencesViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.styles.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.styles[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let style = self.styles[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as! HANameValueCell
        cell.nameLabel.text = NSLocalizedString(style.name, comment: "")
        cell.accessoryType = UserDefaults.standard.angleFormatStyle == style
            ? .checkmark
            : .none
        
        switch indexPath.section {
        case 0:
            cell.valueLabel.text = nil
            
        case 1:
            let formatter = HAAngleFormatter()
            formatter.style = style
            cell.valueLabel.text = formatter.string(from: 0.409092)
            
        default:
            break
        }
        
        return cell
    }
}


// MARK: - Data formatting

extension HAAngleFormatter.Style {
    // Name of the angular format style.
    public var name: String {
        switch self {
        case .standard:
            return "Proper for coordinate system"
        case .radian:
            return "Radians"
        case .decimalDegree:
            return "Decimal degrees"
        case .sexagesimalDegree:
            return "Hexagecimal degrees"
        case .decimalHour:
            return "Decimal hours"
        case .sexagesimalHour:
            return "Hexagecimal hours"
        }
    }
}


// MARK: - User defaults integration

private let angularMeasureKey = "AngularMeasure"

extension UserDefaults {
    var angleFormatStyle: HAAngleFormatter.Style {
        get {
            let styleName = self.string(forKey: angularMeasureKey) ?? ""
            switch styleName.lowercased() {
            case "radians":
                return .radian
            case "decimal degrees":
                return .decimalDegree
            case "hexagecimal degrees":
                return .sexagesimalDegree
            case "decimal hours":
                return .decimalHour
            case "hexagecimal hours":
                return .sexagesimalHour
            default:
                // default value
                return .standard
            }
        }
        
        set {
            switch newValue {
            case .standard:
                self.removeObject(forKey: angularMeasureKey)
            default:
                self.set(newValue.name, forKey: angularMeasureKey)
            }
        }
    }
}
