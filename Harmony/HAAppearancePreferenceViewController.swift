//
//  HABackgroundPreferenceViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 05.08.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit


class HAAppearancePreferenceViewController: UITableViewController {
    fileprivate let appearances: [HAAppearance] = [
        .plain,
        .stellar
    ]
    
    public init() {
        super.init(style: .grouped)
        self.title = NSLocalizedString("Appearance", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


// MARK: - View lifecycle

extension HAAppearancePreferenceViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.registerNibForCellReuse(named: "PreferenceCell")
    }
}


// MARK: - Table view delegate

extension HAAppearancePreferenceViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let userDefaults = UserDefaults.standard
        userDefaults.appearance = self.appearances[indexPath.row]
        
        // NOTE: reloadSections does not cancel row deselection animation unlike reloadData
        let sections = IndexSet(integer: 0)
        self.tableView.reloadSections(sections, with: .automatic)
    }
}


// MARK: - Table view data source

extension HAAppearancePreferenceViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.appearances.count
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let appearance = self.appearances[indexPath.row]
        let userDefaults = UserDefaults.standard
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as! HANameValueCell
        cell.nameLabel.text = NSLocalizedString(appearance.name, comment: "")
        cell.valueLabel.text = nil
        cell.accessoryType = appearance == userDefaults.appearance
            ? .checkmark
            : .none
        
        return cell
    }
}


// MARK: - Appearances

enum HAAppearance {
    case plain
    case stellar
}

extension HAAppearance {
    // Name of the appearance.
    public var name: String {
        switch self {
        case .plain:
            return "Plain"
        case .stellar:
            return "Stellar"
        }
    }
}


// MARK: - User defaults integration

private let appearanceKey = "Appearance"

extension UserDefaults {
    var appearance: HAAppearance {
        get {
            let appearance = self.string(forKey: appearanceKey) ?? ""
            
            switch appearance.lowercased() {
            case "plain":
                return .plain
            case "stellar":
                return .stellar
            default:
                // default value
                return .stellar
            }
        }
        
        set {
            self.set(newValue.name, forKey: appearanceKey)
        }
    }
}
