//
//  HAAppDelegate.swift
//  Harmony
//
//  Created by Сергей Цыба on 27.04.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation
import Ascension


@UIApplicationMain class HAApplicationDelegate: UIResponder {
    var window: UIWindow?
}


// MARK: - Application delegate

extension HAApplicationDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.migratePreferences()
        self.setUpAscension()
        
        let window = UIWindow()
        window.rootViewController = HABodyDataPageViewController()
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }
}


// MARK: - Housekeeping

private let preferencesVersionKey = "PreferencesVersion"
private let currentPreferencesVersion = 2

private extension HAApplicationDelegate {
    func setUpAscension() {
        // note: crash if resource is absent
        ASData.basePath = Bundle.main.path(forResource: "Data", ofType: nil)!
    }
    
    private func migrateFromPreferencesV1() {
        let userDefaults = UserDefaults.standard
        
        guard userDefaults.integer(forKey: preferencesVersionKey) < currentPreferencesVersion else {
            // preferences are already at the latest supported version
            return
        }
        
        // convert from shown body bool array
        if let showBodies = userDefaults.object(forKey: "ShowBodies") as? [Bool] {
            var bodies: [HACelestialBody] = []
            for (index, show) in showBodies.enumerated() {
                if show {
                    bodies.append(HACelestialBody.all[index])
                }
            }
            
            userDefaults.shownBodies = bodies
        }
        
        // convert from angular measure integer keys
        if let angularUnits = userDefaults.object(forKey: "AngularUnits") as? Int {
            switch angularUnits {
            case 0:
                userDefaults.angleFormatStyle = HAAngleFormatter.Style.sexagesimalDegree
            case 1:
                userDefaults.angleFormatStyle = HAAngleFormatter.Style.decimalDegree
            case 2:
                userDefaults.angleFormatStyle = HAAngleFormatter.Style.radian
            default:
                userDefaults.removeObject(forKey: "AngularUnits")
            }
        }
        
        // convert from coordinate system integer keys
        if let coordinateSystem = userDefaults.object(forKey: "CoordinateSystem") as? Int {
            switch coordinateSystem {
            case 0, 1:
                // also move from legacy ecliptic coordinates to equatorial
                userDefaults.coordinateSystem = HACoordinateSystem.localEquatorial
            case 2:
                userDefaults.coordinateSystem = HACoordinateSystem.horizontal
            default:
                userDefaults.removeObject(forKey: "CoordinateSystem")
            }
        }
        
        // convert preferred location keys to new values only if they were specified
        // manually (AutodetectLocation set to false)
        if let latitude = userDefaults.object(forKey: "LocationLatitude") as? CLLocationDegrees,
            let longitude = userDefaults.object(forKey: "LocationLongitude") as? CLLocationDegrees,
            userDefaults.bool(forKey: "AutoDetectLocation") == false {
            userDefaults.preferredLocation = CLLocation(latitude: latitude, longitude: longitude)
        }
        
        // remove legacy user defaults keys
        userDefaults.removeObject(forKey: "Position")
        userDefaults.removeObject(forKey: "AngularUnits")
        userDefaults.removeObject(forKey: "DistanceUnits")
        userDefaults.removeObject(forKey: "AutoDetectLocation")
        userDefaults.removeObject(forKey: "LocationCity")
        userDefaults.removeObject(forKey: "LocationLatitude")
        userDefaults.removeObject(forKey: "LocationLongitude")
    }
    
    func migratePreferences() {
        let version = UserDefaults.standard.integer(forKey: preferencesVersionKey)
        switch version {
        case 0, 1:
            self.migrateFromPreferencesV1()
        default:
            // NOTE: do not mark new preferences version when current version is not recognized
            return
        }
        
        // mark new preferences version
        UserDefaults.standard.set(currentPreferencesVersion, forKey: preferencesVersionKey)
    }
}
