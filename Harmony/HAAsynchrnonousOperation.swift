//
//  HAAsynchrnonousOperation.swift
//  Harmony
//
//  Created by Сергей Цыба on 29.04.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import Foundation

enum HAOperationState {
    case ready
    case executing
    case finished
}

class HAAsynchronousOperation: Operation {
    var state: HAOperationState = .ready {
        willSet {
            switch newValue {
            case .ready:
                assertionFailure("Cannot switch operation state from \(self.state) to \(newValue).")
                
            case .executing:
                self.willChangeValue(forKey: "isExecuting")
                
            case .finished:
                if self.state == .executing {
                    self.willChangeValue(forKey: "isExecuting")
                }
                
                self.willChangeValue(forKey: "isFinished")
            }
        }
        
        didSet {
            switch self.state {
            case .ready:
                assertionFailure("Cannot switch operation state from \(oldValue) to \(self.state).")
                
            case .executing:
                self.didChangeValue(forKey: "isExecuting")
                
            case .finished:
                if oldValue == .executing {
                    self.didChangeValue(forKey: "isExecuting")
                }
                
                self.didChangeValue(forKey: "isFinished")
            }
        }
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isExecuting: Bool {
        return self.state == .executing
    }
    
    override var isFinished: Bool {
        return self.state == .finished
    }
}
