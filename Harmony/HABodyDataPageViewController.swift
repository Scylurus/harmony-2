//
//  HADataPagesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 18.04.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation
import Ascension


class HABodyDataPageViewController: UIViewController {
    @IBOutlet var backgroundView: HASpaceView!
    @IBOutlet var preferencesButton: UIButton!
    
    fileprivate lazy var pageViewController: UIPageViewController = {
        let pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: [:])
        pageViewController.delegate = self
        pageViewController.dataSource = self
        return pageViewController
    }()
    
    fileprivate var bodyDataViewControllers: [HABodyDataViewController] = [] {
        didSet {
            // show same body data view controller as was shown before, if it is present among
            // the newly set body data view controllers; otherwise show the first
            if let currentViewController = self.pageViewController.viewControllers?.first as? HABodyDataViewController,
                let index = self.bodyDataViewControllers.index(of: currentViewController) {
                let viewController = self.bodyDataViewControllers[index]
                self.pageViewController.setViewControllers([viewController], direction: .forward, animated: false, completion: nil)
                self.currentPageIndex = index
            }
            else if let viewController = self.bodyDataViewControllers.first {
                self.pageViewController.setViewControllers([viewController], direction: .forward, animated: false, completion: nil)
                self.currentPageIndex = 0
            }
            
            // update background view to account for new body data view controller count
            if self.isViewLoaded {
                self.updateBackgroundView()
            }
        }
    }
    
    fileprivate var appearance: HAAppearance = .plain {
        didSet {
            if self.isViewLoaded {
                self.updateAppearance()
            }
        }
    }
    
    fileprivate var currentPageIndex = 0
    fileprivate let operationQueue = OperationQueue()
    fileprivate var locationDetermination: HALocationDetermination?
    
    public init() {
        super.init(nibName: "BodyDataPagesView", bundle: .main)
        self.addChildViewController(self.pageViewController)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


// MARK: - Appearance management

extension HAAppearance {
    var foregroundColor: UIColor {
        switch self {
        case .plain:
            return .black
        case .stellar:
            return .white
        }
    }
}

private extension HABodyDataPageViewController {
    func updateBackgroundView() {
        // update background view content size
        let pageCount = CGFloat(self.bodyDataViewControllers.count)
        self.backgroundView.contentSize = CGSize(width: pageCount * self.view.bounds.width, height: self.view.bounds.height)
        
        // update background view content offset
        var offset = self.backgroundView.contentOffset
        offset.x = CGFloat(self.currentPageIndex) * self.backgroundView.bounds.width
        self.backgroundView.contentOffset = offset
        
        self.backgroundView.setNeedsLayout()
    }
    
    func updateAppearance() {
        // update background view appearance
        switch self.appearance {
        case .plain:
            self.backgroundView.isHidden = true
            self.pageViewController.scrollView?.delegate = nil
            
        default:
            self.backgroundView.isHidden = false
            self.pageViewController.scrollView?.delegate = self
        }
        
        // update status bar appearance
        self.setNeedsStatusBarAppearanceUpdate()
        
        // update page control appearance
        let pageControlAppearance = UIPageControl.appearance()
        pageControlAppearance.currentPageIndicatorTintColor = self.appearance.foregroundColor
        pageControlAppearance.pageIndicatorTintColor = pageControlAppearance.currentPageIndicatorTintColor?.withAlphaComponent(0.5)
        
        // update body data view controllers appearance
        for bodyDataViewController in self.bodyDataViewControllers {
            bodyDataViewController.appearance = self.appearance
        }
    }
}


// MARK: - Preference management

private extension Selector {
    static let presentPreferences = #selector(HABodyDataPageViewController.presentPreferencesViewController)
    static let dismissPreferences = #selector(HABodyDataPageViewController.dismissPreferencesViewController)
}

private extension HABodyDataPageViewController {
    @IBAction func presentPreferencesViewController() {
        let preferencesViewController = HAPreferencesViewController()
        preferencesViewController.shouldShowLocationPreferences = (self.locationDetermination?.result == nil)
        preferencesViewController.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: .dismissPreferences)
        
        let navigationController = UINavigationController(rootViewController: preferencesViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    /// Returns an existing data view controller for the specified body if one is currently
    /// showing, or creates a new one otherwise.
    private func dequeueBodyDataViewController(for body: HACelestialBody) -> HABodyDataViewController {
        // reuse body data view controller, if present
        for bodyDataViewController in self.bodyDataViewControllers {
            if bodyDataViewController.body == body {
                return bodyDataViewController
            }
        }
        
        let bodyDataViewController = HABodyDataViewController(for: body)
        bodyDataViewController.operationQueue = self.operationQueue
        
        return bodyDataViewController
    }
    
    func applyPreferences() {
        let userDefaults = UserDefaults.standard
        
        // udpate appearance
        self.appearance = userDefaults.appearance
        
        // update body data view controllers
        self.bodyDataViewControllers = userDefaults.shownBodies.map {
            let bodyDataViewController = self.dequeueBodyDataViewController(for: $0)
            bodyDataViewController.coordinateSystem = userDefaults.coordinateSystem
            bodyDataViewController.angleFormatStyle = userDefaults.angleFormatStyle
            bodyDataViewController.appearance = userDefaults.appearance
            return bodyDataViewController
        }
    }
    
    @IBAction func dismissPreferencesViewController() {
        self.dismiss(animated: true, completion: nil)
        
        self.applyPreferences()
        self.updateBodiesData()
    }
}


// MARK: - Location preferences delegate

extension HABodyDataPageViewController: HALocationPreferencesDelegate {
    func preferencesViewController(_ viewController: UIViewController, didSpecifyLocation location: CLLocation) {
        UserDefaults.standard.preferredLocation = location
        
        self.dismiss(animated: true, completion: nil)
        self.updateBodiesData()
    }
}


// MARK: - Location determination and body data computations

private extension Selector {
    static let updateBodiesData = #selector(HABodyDataPageViewController.updateLocationAndBodiesData)
    static let cancelBodiesDataUpdate = #selector(HABodyDataPageViewController.cancelLocationAndBodiesDataUpdate)
}

private extension HABodyDataPageViewController {
    private func presentLocationPreferencesViewController() {
        let locationPreferencesViewController = HALocationPreferencesViewController()
        locationPreferencesViewController.delegate = self
        
        let navigationController = UINavigationController(rootViewController: locationPreferencesViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    /// Returns an instance of preferences or location preferences view controller if it is
    /// currently being presented inside a navigation controller.
    private var presentedPreferencesViewController: UIViewController? {
        if let navigationController = self.presentedViewController as? UINavigationController {
            for viewController in navigationController.viewControllers {
                if viewController is HAPreferencesViewController || viewController is HALocationPreferencesViewController {
                    return viewController
                }
            }
        }
        
        return nil
    }
    
    /// Hides or shows location preferences section in a preferences view controller when it is
    /// presented.
    private func updateLocationPreferencesViewController(showLocationPreferences: Bool) {
        if let presentedViewController = self.presentedPreferencesViewController {
            if let preferencesViewController = presentedViewController as? HAPreferencesViewController {
                // hide or show location preferences section if preferences are showing
                preferencesViewController.shouldShowLocationPreferences = showLocationPreferences
            }
            else if presentedViewController is HALocationPreferencesViewController {
                // hide location preferenes if they are showing and location was determined
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func updateBodiesData() {
        if let location = self.locationDetermination?.result
            ?? UserDefaults.standard.preferredLocation {
            
            let observation = ASGeoObservation(on: Date(), at: location)
            for bodyDataViewController in self.bodyDataViewControllers {
                bodyDataViewController.showData(for: observation)
            }
        }
        else {
            self.presentLocationPreferencesViewController()
        }
    }
    
    @IBAction func updateLocationAndBodiesData() {
        let locationDetermination = HALocationDetermination()
        locationDetermination.qualityOfService = .utility
        
        self.locationDetermination?.cancel()
        self.locationDetermination = locationDetermination
        OperationQueue.main.addOperation(locationDetermination)
        
        let completion = BlockOperation() {
            guard locationDetermination.isCancelled == false else {
                return
            }
            
            if self.isViewLoaded {
                self.updateLocationPreferencesViewController(showLocationPreferences: locationDetermination.result == nil)
            }
            
            self.updateBodiesData()
        }
        
        completion.qualityOfService = .userInteractive
        completion.addDependency(locationDetermination)
        OperationQueue.main.addOperation(completion)
    }
    
    @IBAction func cancelLocationAndBodiesDataUpdate() {
        self.locationDetermination?.cancel()
        self.operationQueue.cancelAllOperations()
    }
}


// MARK: - View lifecycle

extension HABodyDataPageViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.insertSubview(self.pageViewController.view, at: 1)
        self.view.addConstraints([
            NSLayoutConstraint(item: self.view, attribute: .top, relatedBy: .equal, toItem: self.pageViewController.view, attribute: .top, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self.view, attribute: .trailing, relatedBy: .equal, toItem: self.pageViewController.view, attribute: .trailing, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self.view, attribute: .bottom, relatedBy: .equal, toItem: self.pageViewController.view, attribute: .bottom, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self.view, attribute: .leading, relatedBy: .equal, toItem: self.pageViewController.view, attribute: .leading, multiplier: 1.0, constant: 0.0)
            ])
        
        self.applyPreferences()
        self.updateLocationAndBodiesData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: .updateBodiesData, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        notificationCenter.addObserver(self, selector: .cancelBodiesDataUpdate, name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        switch self.appearance {
        case .plain:
            return .default
        case .stellar:
            return .lightContent
        }
    }
}


// MARK: - Page view controller delegate

extension HABodyDataPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let bodyDataViewController = pageViewController.viewControllers?.first as? HABodyDataViewController,
                let index = self.bodyDataViewControllers.index(of: bodyDataViewController) {
                self.currentPageIndex = index
            }
        }
    }
}


// MARK: - Page view controller data source

extension HABodyDataPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let bodyDataViewController = viewController as? HABodyDataViewController,
            let index = self.bodyDataViewControllers.index(of: bodyDataViewController),
            index > 0 {
            return self.bodyDataViewControllers[index - 1]
        }
        else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let bodyDataViewController = viewController as? HABodyDataViewController,
            let index = self.bodyDataViewControllers.index(of: bodyDataViewController),
            index < self.bodyDataViewControllers.count - 1 {
            return self.bodyDataViewControllers[index + 1]
        }
        else {
            return nil
        }
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.bodyDataViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let bodyDataViewController = pageViewController.viewControllers?.first as? HABodyDataViewController,
            let index = self.bodyDataViewControllers.index(of: bodyDataViewController) {
            return index
        }
        else {
            return 0
        }
    }
}


// MARK: - Scroll view delegate

extension HABodyDataPageViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var offset = scrollView.contentOffset
        offset.x += CGFloat(self.currentPageIndex - 1) * scrollView.bounds.width
        
        self.backgroundView.contentOffset = offset
    }
}


// MARK: - Convenience functionality

private extension UIPageViewController {
    var scrollView: UIScrollView? {
        for subview in self.view.subviews {
            if let scrollView = subview as? UIScrollView {
                return scrollView
            }
        }
        
        return nil
    }
}


// MARK: - Ascension integration

extension Date {
    /// Julian date of reference date of the Foundation Date object.
    private static let referenceJulianDate: ASJulianDate = 2451910.5
    
    /// Creates date from the specified Julian date.
    public init(julianDate: ASJulianDate) {
        self.init(timeIntervalSinceReferenceDate: (julianDate - Date.referenceJulianDate) * 86400.0)
    }
    
    /// Returns Juliand date of the date.
    public var julianDate: ASJulianDate {
        return self.timeIntervalSinceReferenceDate / 86400.0 + Date.referenceJulianDate
    }
}

private extension ASGeographicPoint {
    init(_ location: CLLocation) {
        let longitude = ASAngle(degrees: location.coordinate.longitude)
        let latitude = ASAngle(degrees: location.coordinate.latitude)
        self.init(longitude: longitude, latitude: latitude, altitude: location.altitude)
    }
}

private extension ASGeoObservation {
    convenience init(on date: Date, at location: CLLocation) {
        let point = ASGeographicPoint(location)
        self.init(on: date.julianDate, at: point.itrfPosition)
    }
}
