//
//  HABodyDataViewController2.swift
//  Harmony
//
//  Created by Сергей Цыба on 31.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation
import Ascension


class HABodyDataViewController: UIViewController {
    @IBOutlet var bodyNameLabel: UILabel!
    
    @IBOutlet var positionVelocityLabels: [UILabel]!
    @IBOutlet var velocityNameLabel: UILabel!
    @IBOutlet var positionContainerView: UIView!
    @IBOutlet var velocityContainerView: UIView!
    
    @IBOutlet var horizontalStateLabels: [UILabel]!
    @IBOutlet var horizontalStateContainerView: UIView!
    @IBOutlet var horizontalStateSpecialLabel: UILabel!
    
    @IBOutlet var separators: [NSLayoutConstraint]!
    
    public let body: HACelestialBody
    
    public var coordinateSystem: HACoordinateSystem = .horizontal
    public var angleFormatStyle: HAAngleFormatter.Style = .standard
    public var appearance: HAAppearance = .plain {
        didSet {
            if self.isViewLoaded {
                self.updateAppearance()
            }
        }
    }
    
    public var operationQueue = OperationQueue()
    fileprivate var terrestrialStateComputation: HATerrestrialStateComputation?
    fileprivate var horizontalStateComputation: HAHorizontalStateComputation?
    
    public required init(for body: HACelestialBody) {
        self.body = body
        
        super.init(nibName: "BodyDataView", bundle: .main)
        self.title = NSLocalizedString(body.name, comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


// MARK: - View lifecycle

extension HABodyDataViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // halve space between container views when running on a 3.5 inch device
        let screen = UIScreen.main
        if max(screen.bounds.width, screen.bounds.height) < 500.0 {
            for separator in separators {
                separator.constant /= 2.0
            }
            
            self.view.invalidateIntrinsicContentSize()
        }
        
        self.bodyNameLabel.text = NSLocalizedString(self.body.name, comment: "")
        self.velocityNameLabel.text = NSLocalizedString("velocity", comment: "")
        
        self.updateAppearance()
        self.showTerrestrialStateComputationResult()
        self.showHorizontalStateComputationResult()
        
        // all shown dates are formatted relative to the current day; thus, all dates need to be
        // re-formatted when day changes
        NotificationCenter.default.addObserver(self, selector: .updateFormattedDates, name: NSNotification.Name.NSCalendarDayChanged, object: nil)
    }
}


// MARK: - Appearance management

private extension HAAppearance {
    var backgroundColor: UIColor {
        return .clear
    }
}

private extension HABodyDataViewController {
    func updateAppearance() {
        let backgroundColor = self.appearance.backgroundColor
        self.view.backgroundColor = backgroundColor
        self.positionContainerView.backgroundColor = backgroundColor
        self.velocityContainerView.backgroundColor = backgroundColor
        self.horizontalStateContainerView.backgroundColor = backgroundColor
        
        let textColor = self.appearance.foregroundColor
        self.bodyNameLabel.textColor = textColor
        for label in self.positionVelocityLabels {
            label.textColor = textColor
        }
        self.velocityNameLabel.textColor = textColor
        for label in self.horizontalStateLabels {
            label.textColor = textColor
        }
        
        self.horizontalStateSpecialLabel.textColor = textColor
    }
}


// MARK: - Formatting and showing data

private extension Selector {
    static let updateFormattedDates = #selector(HABodyDataViewController.updateFormattedDates)
}

private extension HABodyDataViewController {
    @IBAction func updateFormattedDates() {
        OperationQueue.main.addOperation() {
            self.showHorizontalStateComputationResult()
        }
    }
}

private extension HABodyDataViewController {
    private func show(formattedPosition: NSAttributedString, formattedVelocity: NSAttributedString) {
        let string = NSMutableAttributedString()
        string.append(formattedPosition)
        string.append(formattedVelocity)
        
        var textValues: [String] = []
        
        let range = NSMakeRange(0, string.length)
        string.enumerateAttribute("localizedOrdinateName", in: range, options: []) { value, range, stop in
            if let ordinateName = value as? String {
                let substring = string.attributedSubstring(from: range)
                textValues.append(ordinateName)
                textValues.append(substring.string)
            }
        }
        
        for (label, textValue) in zip(self.positionVelocityLabels, textValues) {
            label.text = textValue
        }
    }
    
    func showTerrestrialStateComputationResult() {
        if let state = self.terrestrialStateComputation?.result {
            let formatted: (position: NSAttributedString, velocity: NSAttributedString)
            
            switch self.coordinateSystem {
            case .horizontal:
                let formatter = HAHorizontalPointFormatter()
                formatter.angleStyle = self.angleFormatStyle
                
                let (position, velocity) = state.horizontalValues
                formatted = (
                    formatter.attributedString(from: position),
                    formatter.attributedString(from: velocity)
                )
                
            case .localEquatorial:
                let formatter = HALocalEquatorialPointFormatter()
                formatter.angleStyle = self.angleFormatStyle
                
                let (position, velocity) = state.localEquatorialValues
                formatted = (
                    formatter.attributedString(from: position),
                    formatter.attributedString(from: velocity)
                )
            }
            
            self.show(formattedPosition: formatted.position, formattedVelocity: formatted.velocity)
            self.positionContainerView.isHidden = false
            self.velocityContainerView.isHidden = false
        }
        else {
            self.positionContainerView.isHidden = true
            self.velocityContainerView.isHidden = true
        }
    }
    
    func showHorizontalStateComputationResult() {
        if let state = self.horizontalStateComputation?.result {
            let formatter = HAHorizontalStateFormatter()
            let formatted = formatter.attributedString(from: state)
            
            var textValues: [String] = []
            
            let range = NSMakeRange(0, formatted.length)
            formatted.enumerateAttributes(in: range, options: []) { attributes, range, stop in
                if let _ = attributes["propertyName"] {
                    let substring = formatted.attributedSubstring(from: range)
                    textValues.append(substring.string)
                }
            }
            
            if textValues.count == 1 {
                self.horizontalStateSpecialLabel.text = textValues.first
                
                self.horizontalStateContainerView.isHidden = true
                self.horizontalStateSpecialLabel.isHidden = false
            }
            else {
                for (index, label) in self.horizontalStateLabels.enumerated() {
                    label.text = index < textValues.count
                        ? textValues[index]
                        : nil
                }
                
                self.horizontalStateContainerView.isHidden = false
                self.horizontalStateSpecialLabel.isHidden = true
            }
        }
        else {
            self.horizontalStateContainerView.isHidden = true
            self.horizontalStateSpecialLabel.isHidden = true
        }
    }
}


// MARK: - Computing data

extension HABodyDataViewController {
    private func computePositionVelocity(for observation: ASGeoObservation) {
        let computation = HATerrestrialStateComputation(of: self.body.ascensionObject, at: observation)
        computation.queuePriority = .high
        computation.qualityOfService = .utility
        
        self.terrestrialStateComputation?.cancel()
        self.terrestrialStateComputation = computation
        self.operationQueue.addOperation(computation)
        
        let completion = BlockOperation() {
            guard computation.isCancelled == false,
                let _ = computation.result else {
                    return
            }
            
            if self.isViewLoaded {
                self.showTerrestrialStateComputationResult()
            }
        }
        
        completion.qualityOfService = .userInteractive
        completion.addDependency(computation)
        OperationQueue.main.addOperation(completion)
    }
    
    private func computeHorizontalState(for observation: ASGeoObservation) {
        let computation = HAHorizontalStateComputation(of: self.body.ascensionObject, at: observation)
        computation.queuePriority = .low
        computation.qualityOfService = .utility
        
        self.horizontalStateComputation?.cancel()
        self.horizontalStateComputation = computation
        self.operationQueue.addOperation(computation)
        
        let completion = BlockOperation() {
            guard computation.isCancelled == false,
                let _ = computation.result else {
                    return
            }
            
            if self.isViewLoaded {
                self.showHorizontalStateComputationResult()
            }
        }
        
        completion.qualityOfService = .userInteractive
        completion.addDependency(computation)
        OperationQueue.main.addOperation(completion)
    }
    
    /// Checks whether it is necessary to compute horizontal state or a previously computed result
    /// is valid for the specified observation.
    ///
    /// Computed rise and set dates are same for the specified dates between them and locations
    /// close to the one used previously for computation.
    private func canReuseHorizontalStateComputationResult(for observation: ASGeoObservation) -> Bool {
        guard let operation = self.horizontalStateComputation,
            let state = operation.result else {
                return false
        }
        
        let position1 = ASGeographicPoint(itrfPosition: operation.observation.geoPosition)
        let position2 = ASGeographicPoint(itrfPosition: observation.geoPosition)
        if position1.distance(to: position2) > 3000.0 {
            return false
        }
        
        let date = Date(julianDate: observation.date)
        switch state {
        case .aboveHorizon(let riseDate, let setDate):
            return riseDate?.compare(date) == .orderedAscending
                && setDate?.compare(date) == .orderedDescending
            
        case .belowHorizon(let setDate, let riseDate):
            return setDate?.compare(date) == .orderedAscending
                && riseDate?.compare(date) == .orderedDescending
        }
    }
    
    public func showData(for observation: ASGeoObservation) {
        self.computePositionVelocity(for: observation)
        
        if self.canReuseHorizontalStateComputationResult(for: observation) {
            // refresh view to update formatted labels for rise and set dates, since day number
            // may have changed from they were displayed last time
            if self.isViewLoaded {
                self.showHorizontalStateComputationResult()
            }
        }
        else {
            self.computeHorizontalState(for: observation)
        }
    }
}


// MARK: - Convenience functionality

struct HALocalEquatorialPoint {
    var hourAngle: ASAngle
    var declination: ASAngle
    var distance: Double
}

private extension ASTerrestrialState {
    var localEquatorialValues: (position: HALocalEquatorialPoint, velocity: HALocalEquatorialPoint) {
        let (position, velocity) = self.equatorialValues
        
        return (
            HALocalEquatorialPoint(
                hourAngle: self.hourAngle,
                declination: position.declination,
                distance: position.distance
            ),
            HALocalEquatorialPoint(
                hourAngle: -velocity.rightAscension,
                declination: velocity.declination,
                distance: velocity.distance
            )
        )
    }
}


// MARK: - Ascension integration

extension HACelestialBody {
    var ascensionObject: ASCelestialObject {
        switch self {
        case .sun:
            return ASGeoLuminary.sun
        case .moon:
            return ASGeoLuminary.moon
        case .mercury:
            return ASSolarPlanet.mercury
        case .venus:
            return ASSolarPlanet.venus
        case .mars:
            return ASSolarPlanet.mars
        case .jupiter:
            return ASSolarPlanet.jupiter
        case .saturn:
            return ASSolarPlanet.saturn
        case .uranus:
            return ASSolarPlanet.uranus
        case .neptune:
            return ASSolarPlanet.neptune
        }
    }
}
