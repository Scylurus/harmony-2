//
//  HACoordinateSystemPreferencesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 05.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit


class HACoordinateSystemPreferencesViewController: UITableViewController {
    fileprivate let coordinateSystems: [HACoordinateSystem] = [
        .horizontal,
        .localEquatorial
    ]
    
    public init() {
        super.init(style: .grouped)
        self.title = NSLocalizedString("Coordinate system", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: - View lifecycle

extension HACoordinateSystemPreferencesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.registerNibForCellReuse(named: "PreferenceCell")
    }
}


// MARK: - Table view delegate

extension HACoordinateSystemPreferencesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        UserDefaults.standard.coordinateSystem = self.coordinateSystems[indexPath.row]
        
        // NOTE: `reloadSections` does not cancel row deselection animation, unlike `reloadData`
        let sections = IndexSet(integer: 0)
        tableView.reloadSections(sections, with: .automatic)
    }
}


// MARK: - Table view data source

extension HACoordinateSystemPreferencesViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coordinateSystems.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let coordinateSystem = self.coordinateSystems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as! HANameValueCell
        cell.nameLabel.text = NSLocalizedString(coordinateSystem.name, comment: "")
        cell.valueLabel.text = nil
        cell.accessoryType = coordinateSystem == UserDefaults.standard.coordinateSystem
            ? .checkmark
            : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return NSLocalizedString("All values are referred to the ITRF.", comment: "")
    }
}


// MARK: - Coordinate systems

enum HACoordinateSystem {
    case horizontal
    case localEquatorial
}

extension HACoordinateSystem {
    /// Name of the coordinate system.
    var name: String {
        switch self {
        case .horizontal:
            return "Horizontal"
        case .localEquatorial:
            return "Local equatorial"
        }
    }
}


// MARK: - User defaults integration

private let coordinateSystemKey = "CoordinateSystem"

extension UserDefaults {
    var coordinateSystem: HACoordinateSystem {
        get {
            let coordinateSystemName = self.string(forKey: coordinateSystemKey) ?? ""
            switch coordinateSystemName.lowercased() {
            case "horizontal":
                return .horizontal
            case "local equatorial",
                 "equatorial":
                // NOTE: temporarily show local equatorial coordinates for equatorial in order to
                // preserve user preferences until proper equatorial coordinates computations are
                // ready
                return .localEquatorial
            default:
                // default value
                return .horizontal
            }
        }
        
        set {
            self.set(newValue.name, forKey: coordinateSystemKey)
        }
    }
}
