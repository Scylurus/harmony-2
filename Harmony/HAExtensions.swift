//
//  Extensions.swift
//  Harmony
//
//  Created by Сергей Цыба on 03.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit


extension UITableView {
    func registerNibForCellReuse(named name: String, from bundle: Bundle = Bundle.main) {
        let nib = UINib(nibName: name, bundle: bundle)
        self.register(nib, forCellReuseIdentifier: name)
    }
}
