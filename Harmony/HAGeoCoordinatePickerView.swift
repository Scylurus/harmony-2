//
//  HAGeoCoordinatePickerView.swift
//  Harmony
//
//  Created by Сергей Цыба on 10.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation


@IBDesignable class HAGeoCoordinatePicker: UIControl {
    enum Mode {
        case longitudinal
        case latitudinal
    }
    
    fileprivate lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()
    
    public var mode: Mode = .longitudinal {
        didSet {
            self.pickerView.reloadAllComponents()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.pickerView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.pickerView)
        self.addConstraints([
            NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: self.pickerView, attribute: .top, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: self.pickerView, attribute: .trailing, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: self.pickerView, attribute: .bottom, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: self.pickerView, attribute: .leading, multiplier: 1.0, constant: 0.0)
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addSubview(self.pickerView)
    }
}


// MARK: - View geometry processing

extension HAGeoCoordinatePicker {
    override var intrinsicContentSize : CGSize {
        return self.pickerView.intrinsicContentSize
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.pickerView.center = self.center
    }
}


// MARK: - Custom functionality

extension HAGeoCoordinatePicker {
    /// Value currently displayed by the picker.
    public var value: CLLocationDegrees {
        get {
            let degrees = self.pickerView.selectedRow(inComponent: 0)
            let minutes = self.pickerView.selectedRow(inComponent: 1)
            let seconds = self.pickerView.selectedRow(inComponent: 2)
            var value = Double(degrees) + (Double(minutes) + Double(seconds) / 60.0) / 60.0
            
            // in case coordinate value is zero, setting it to the minimum posible will preserve
            // its sign, i.e. differentiate between hemispheres
            if value == 0.0 {
                value = DBL_MIN
            }
            
            let row = self.pickerView.selectedRow(inComponent: 3)
            switch row {
            case 0:
                return value
            case 1:
                return -value
            default:
                assertionFailure("Unexpected row number \(row) in hemisphere component.")
                return 0.0
            }
        }
        
        set {
            self.setValue(newValue, animated: false)
        }
    }
    
    /// Shows the specified value. Transition the specified value is animated, if `animated` is
    /// set to true and is not otherwise.
    public func setValue(_ value: CLLocationDegrees, animated: Bool) {
        let (degrees, minutes, seconds) = roundedSexagesimalParts(of: fabs(value))
        let hemisphere = value < 0 ? 1 : 0
        
        self.pickerView.selectRow(degrees, inComponent: 0, animated: animated)
        self.pickerView.selectRow(minutes, inComponent: 1, animated: animated)
        self.pickerView.selectRow(seconds, inComponent: 2, animated: animated)
        self.pickerView.selectRow(hemisphere, inComponent: 3, animated: animated)
    }
}


// MARK: - Picker view data source

extension HAGeoCoordinatePicker: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            switch self.mode {
            case .longitudinal:
                return 180
            case .latitudinal:
                return 90
            }
        case 1:
            return 60
        case 2:
            return 60
        case 3:
            return 2
        default:
            return 0
        }
    }
}


// MARK: - Picker view delegate

extension HAGeoCoordinatePicker: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch  component {
        case 0:
            return "\(row)°"
        case 1:
            return row < 10 ? "0\(row)′" : "\(row)′"
        case 2:
            return row < 10 ? "0\(row)″" : "\(row)″"
            
        case 3:
            switch (self.mode, row) {
            case (.longitudinal, 0):
                return NSLocalizedString("E", comment: "Eastern hemisphere designator.")
            case (.longitudinal, 1):
                return NSLocalizedString("W", comment: "Western hemisphere designator.")
            case (.latitudinal, 0):
                return NSLocalizedString("N", comment: "Northern hemisphere designator.")
            case (.latitudinal, 1):
                return NSLocalizedString("S", comment: "Southern hemisphere designator.")
            default:
                return nil
            }
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.sendActions(for: .valueChanged)
    }
}
