//
//  HAComputeHorizontalState.swift
//  Harmony
//
//  Created by Сергей Цыба on 29.04.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import Foundation
import Ascension


// MARK: Horizontal state

enum HAHorizontalState {
    case aboveHorizon(riseDate: Date?, setDate: Date?)
    case belowHorizon(setDate: Date?, riseDate: Date?)
}


class HAHorizontalStateComputation: HAAsynchronousOperation {
    public let object: ASCelestialObject
    public let observation: ASGeoObservation
    public private(set) var result: HAHorizontalState?
    
    public init(of object: ASCelestialObject, at observation: ASGeoObservation) {
        self.object = object
        self.observation = observation
    }
    
    override func start() {
        guard self.isCancelled == false else {
            self.state = .finished
            return
        }
        
        self.state = .executing
        
        let riseCondition = self.observation.rise(of: self.object)
        
        guard self.isCancelled == false else {
            self.state = .finished
            return
        }
        
        let setCondition = self.observation.set(of: self.object)
        
        guard self.isCancelled == false else {
            self.state = .finished
            return
        }
        
        assert(riseCondition.horizontalAppearance == setCondition.horizontalAppearance)
        
        var riseDate: Date? = nil
        if let date = riseCondition.date {
            riseDate = Date(julianDate: date)
        }
        
        var setDate: Date? = nil
        if let date = setCondition.date {
            setDate = Date(julianDate: date)
        }
        
        switch riseCondition.horizontalAppearance {
        case .aboveHorizon:
            self.result = .aboveHorizon(riseDate: riseDate, setDate: setDate)
        case .belowHorizon:
            self.result = .belowHorizon(setDate: setDate, riseDate: riseDate)
        }
        
        self.state = .finished
    }
}
