//
//  HAHorizontalAppearanceFormatter.swift
//  Harmony
//
//  Created by Сергей Цыба on 03.10.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import Foundation
import Ascension


class HAHorizontalStateFormatter {
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter
    }()
    
    private let timeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .medium
        return formatter
    }()
    
    private func prefix(forRiseDate date: Date) -> String {
        let calendar = Calendar.current
        
        if calendar.isDateInYesterday(date) {
            return NSLocalizedString("risen yesterday at", comment: "")
        }
        else if calendar.isDateInToday(date) {
            let now = Date()
            return date.compare(now) == .orderedAscending
                ? NSLocalizedString("risen at", comment: "")
                : NSLocalizedString("rises at", comment: "")
        }
        else if calendar.isDateInTomorrow(date) {
            return NSLocalizedString("rises tomorrow at", comment: "")
        }
        else {
            let now = Date()
            let format = date.compare(now) == .orderedAscending
                ? NSLocalizedString("risen on %@ at", comment: "")
                : NSLocalizedString("rises on %@ at", comment: "")
            
            let formattedDate = dateFormatter.string(from: date)
            return String.localizedStringWithFormat(format, formattedDate)
        }
    }
    
    private func prefix(forSetDate date: Date) -> String {
        let calendar = Calendar.current
        
        if calendar.isDateInYesterday(date) {
            return NSLocalizedString("set yesterday at", comment: "")
        }
        else if calendar.isDateInToday(date) {
            let now = Date()
            return date.compare(now) == .orderedAscending
                ? NSLocalizedString("set at", comment: "")
                : NSLocalizedString("sets at", comment: "")
        }
        else if calendar.isDateInTomorrow(date) {
            return NSLocalizedString("sets tomorrow at", comment: "")
        }
        else {
            let now = Date()
            let format = date.compare(now) == .orderedAscending
                ? NSLocalizedString("set on %@ at", comment: "")
                : NSLocalizedString("sets on %@ at", comment: "")
            
            let formattedDate = dateFormatter.string(from: date)
            return String.localizedStringWithFormat(format, formattedDate)
        }
    }
    
    
    /// Formats the specified horizontal state.
    public func string(from state: HAHorizontalState) -> String {
        var strings: [String] = []
        
        if let date = state.riseDate {
            let prefix = self.prefix(forRiseDate: date)
            let formattedTime = self.timeFormatter.string(from: date)
            strings.append("\(prefix) \(formattedTime)")
        }
        
        if let date = state.setDate {
            let prefix = self.prefix(forSetDate: date)
            let formattedTime = self.timeFormatter.string(from: date)
            strings.append("\(prefix) \(formattedTime)")
        }
        
        if strings.isEmpty {
            switch state {
            case .aboveHorizon:
                return NSLocalizedString("up all day", comment: "")
            case .belowHorizon:
                return NSLocalizedString("down all day", comment: "")
            }
        }
        else {
            switch state {
            case .aboveHorizon:
                break
            case .belowHorizon:
                strings = strings.reversed()
            }
            
            return strings.joined(separator: ", ")
        }
    }
    
    
    /// Formats the specified horizontal state as an attributed string.
    public func attributedString(from state: HAHorizontalState) -> NSAttributedString {
        var strings: [NSAttributedString] = []
        
        if let date = state.riseDate {
            let prefix = self.prefix(forRiseDate: date)
            let formattedTime = self.timeFormatter.string(from: date)
            
            let string = NSMutableAttributedString()
            string.append(prefix, withAttributes: ["propertyName": "riseDescription"])
            string.append(" ")
            string.append(formattedTime, withAttributes: ["propertyName": "riseTime"])
            strings.append(string)
        }
        
        if let date = state.setDate {
            let prefix = self.prefix(forSetDate: date)
            let formattedTime = self.timeFormatter.string(from: date)
            
            let string = NSMutableAttributedString()
            string.append(prefix, withAttributes: ["propertyName": "setDescription"])
            string.append(" ")
            string.append(formattedTime, withAttributes: ["propertyName": "setTime"])
            strings.append(string)
        }
        
        if strings.isEmpty {
            let string = NSMutableAttributedString()
            switch state {
            case .aboveHorizon:
                let description = NSLocalizedString("up all day", comment: "")
                string.append(description, withAttributes: ["propertyName": "circumpolarAppearance"])
                
            case .belowHorizon:
                let description = NSLocalizedString("down all day", comment: "")
                string.append(description, withAttributes: ["propertyName": "circumpolarAppearance"])
            }
            
            return string
        }
        else {
            switch state {
            case .aboveHorizon:
                break
            case .belowHorizon:
                strings = strings.reversed()
            }
            
            let separator = NSMutableAttributedString(string: ", ")
            return strings.joined(by: separator)
        }
    }
}


// MARK: - Convenience functionality

private extension HAHorizontalState {
    var riseDate: Date? {
        switch self {
        case .aboveHorizon(let riseDate, _):
            return riseDate
        case .belowHorizon(_, let riseDate):
            return riseDate
        }
    }
    
    var setDate: Date? {
        switch self {
        case .aboveHorizon(_, let setDate):
            return setDate
        case .belowHorizon(let setDate, _):
            return setDate
        }
    }
}

private extension Array where Element: NSAttributedString {
    func joined(by separator: NSAttributedString) -> NSAttributedString {
        let string = NSMutableAttributedString()
        
        if let firstString = self.first {
            string.append(firstString)
        }
        
        for index in 1 ..< self.count {
            string.append(separator)
            string.append(self[index])
        }
        
        return string
    }
}
