//
//  HALocationCoordinatesPreferencesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 08.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation
import Ascension


class HALocationCoordinatesPreferencesViewController: UITableViewController {
    fileprivate lazy var resetBarButtonItem: UIBarButtonItem = {
        let itemTitle = NSLocalizedString("Reset", comment: "")
        let item = UIBarButtonItem(title: itemTitle, style: .plain, target: self, action: .resetCoordinates)
        
        return item
    }()
    
    fileprivate var pickerViewIndexPath = IndexPath(row: 1, section: 0) {
        didSet {
            if self.isViewLoaded {
                let indexPaths = [
                    oldValue,
                    self.pickerViewIndexPath
                ]
                
                self.tableView.reloadRows(at: indexPaths, with: .automatic)
            }
        }
    }
    
    fileprivate let initialCoordinates: CLLocationCoordinate2D
    fileprivate var enteredCoordinates: CLLocationCoordinate2D
    
    // TODO: update tab bar item in `didSet`
    public var shouldAllowResetting = false
    public var shouldCommitOnViewDissapear = false
    public var delegate: HALocationPreferencesDelegate?
    
    public init(showing value: CLLocation) {
        self.initialCoordinates = value.coordinate
        self.enteredCoordinates = value.coordinate
        
        super.init(style: .grouped)
        self.title = NSLocalizedString("Coordinate system", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - Target actions

private extension Selector {
    static let updateCoordinate = #selector(HALocationCoordinatesPreferencesViewController.updateCoordinate(_:))
    static let resetCoordinates = #selector(HALocationCoordinatesPreferencesViewController.resetCoordinates)
}

extension HALocationCoordinatesPreferencesViewController {
    @IBAction func updateCoordinate(_ sender: HAGeoCoordinatePicker!) {
        switch sender.mode {
        case .longitudinal:
            self.enteredCoordinates.longitude = sender.value
        case .latitudinal:
            self.enteredCoordinates.latitude = sender.value
        }
        
        // reload row above the picker view to update displayed value
        let indexPath = IndexPath(row: self.pickerViewIndexPath.row - 1, section: self.pickerViewIndexPath.section)
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
        
        if self.shouldAllowResetting {
            // show reset bar button item if displayed value differs from the initial
            self.navigationItem.rightBarButtonItem = self.enteredCoordinates == self.initialCoordinates
                ? nil
                : self.resetBarButtonItem
        }
    }
    
    @IBAction func resetCoordinates() {
        self.enteredCoordinates = self.initialCoordinates
        
        // reload values in rows other than the one with picker view
        if var indexPaths = self.tableView.indexPathsForVisibleRows,
            let index = indexPaths.index(of: self.pickerViewIndexPath) {
            indexPaths.remove(at: index)
            self.tableView.reloadRows(at: indexPaths, with: .automatic)
        }
        
        // animate value change in picker view
        if let cell = tableView.cellForRow(at: self.pickerViewIndexPath) as? HAGeoCoordinatePickerCell {
            switch self.pickerViewIndexPath.row {
            case 1:
                cell.geoCoordinatePicker.setValue(self.enteredCoordinates.longitude, animated: true)
            case 2:
                cell.geoCoordinatePicker.setValue(self.enteredCoordinates.latitude, animated: true)
            default:
                break
            }
        }
    }
    
    @IBAction func commitCoordinates() {
        let location = CLLocation(latitude: self.enteredCoordinates.latitude, longitude: self.enteredCoordinates.longitude)
        self.delegate?.preferencesViewController(self, didSpecifyLocation: location)
    }
}


// MARK: - View lifecycle

extension HALocationCoordinatesPreferencesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100.0
        
        self.tableView.registerNibForCellReuse(named: "PreferenceCell")
        self.tableView.registerNibForCellReuse(named: "GeoCoordinatePickerCell")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.shouldCommitOnViewDissapear {
            self.commitCoordinates()
        }
    }
}


// MARK: - Table view delegate

extension HALocationCoordinatesPreferencesViewController {
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return indexPath != self.pickerViewIndexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.pickerViewIndexPath = indexPath.row < self.pickerViewIndexPath.row
            ? IndexPath(row: indexPath.row + 1, section: indexPath.section)
            : indexPath
    }
}


// MARK: - Table view data source

extension HALocationCoordinatesPreferencesViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath == self.pickerViewIndexPath {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GeoCoordinatePickerCell") as! HAGeoCoordinatePickerCell
            
            switch indexPath.row {
            case 1:
                cell.geoCoordinatePicker.mode = .longitudinal
                cell.geoCoordinatePicker.value = self.enteredCoordinates.longitude
                
            case 2:
                cell.geoCoordinatePicker.mode = .latitudinal
                cell.geoCoordinatePicker.value = self.enteredCoordinates.latitude
                
            default:
                break
            }
            
            cell.geoCoordinatePicker.addTarget(self, action: .updateCoordinate, for: .valueChanged)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as! HANameValueCell
            
            let geographicPoint = ASGeographicPoint(self.enteredCoordinates)
            let formatter = HAGeographicPointFormatter()
            let string = formatter.attributedString(from: geographicPoint)
            let range = NSMakeRange(0, string.length)
            
            string.enumerateAttributes(in: range, options: []) { attributes, range, stop in
                if let ordinateName = attributes["ordinateName"] as? String {
                    switch (indexPath.row, ordinateName) {
                    case (0, "longitude"),
                         (1, "latitude"),
                         (2, "latitude"):
                        let localizedName = attributes["localizedOrdinateName"] as? String
                        let substring = string.attributedSubstring(from: range)
                        
                        cell.nameLabel.text = localizedName?.capitalized
                        cell.valueLabel.text = substring.string
                        cell.accessoryType = .none
                        
                        stop[0] = true
                        
                    default:
                        break
                    }
                }
            }
            
            return cell
        }
    }
}


// MARK: - Table view cells

class HAGeoCoordinatePickerCell: UITableViewCell {
    @IBOutlet var geoCoordinatePicker: HAGeoCoordinatePicker!
}


// MARK: - Convenience functionality

extension CLLocationCoordinate2D: Equatable {}
public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
    return lhs.latitude == rhs.latitude
        && lhs.longitude == rhs.longitude
}

private extension ASGeographicPoint {
    init(_ coordinates: CLLocationCoordinate2D) {
        let longitude = ASAngle(degrees: coordinates.longitude)
        let latitude = ASAngle(degrees: coordinates.latitude)
        self.init(longitude: longitude, latitude: latitude, altitude: 0.0)
    }
}
