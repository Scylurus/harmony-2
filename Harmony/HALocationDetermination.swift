//
//  HALocationDetermination.swift
//  Harmony
//
//  Created by Сергей Цыба on 09.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import Foundation
import CoreLocation


class HALocationDetermination: HAAsynchronousOperation {
    fileprivate lazy var locationManager = CLLocationManager()
    
    public fileprivate(set) var result: CLLocation? {
        didSet {
            // location location updating in case it was running
            self.locationManager.stopUpdatingLocation()
            
            // clear location manager delegate to avoid responding to any location manager events
            self.locationManager.delegate = nil
            self.state = .finished
        }
    }
    
    override func start() {
        guard self.isCancelled == false else {
            self.state = .finished
            return
        }
        
        self.state = .executing
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
            self.locationManager.delegate = self
            
            let status = CLLocationManager.authorizationStatus()
            self.locationManager(self.locationManager, didChangeAuthorization: status)
        }
        else {
            self.result = nil
        }
    }
}


// MARK: - Location manager delegate

extension HALocationDetermination: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard self.isCancelled == false else {
            manager.stopUpdatingLocation()
            self.result = nil
            
            return
        }
        
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            
        case .restricted, .denied:
            manager.stopUpdatingLocation()
            self.result = nil
            
        case .authorizedWhenInUse, .authorizedAlways:
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard self.isCancelled == false else {
            manager.stopUpdatingLocation()
            self.result = nil
            
            return
        }
        
        if let location = locations.last, location.horizontalAccuracy <= manager.desiredAccuracy {
            guard location.horizontalAccuracy >= 0.0 else {
                return
            }
            
            manager.stopUpdatingLocation()
            self.result = location
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        guard self.isCancelled == false else {
            manager.stopUpdatingLocation()
            self.result = nil
            
            return
        }
        
        manager.stopUpdatingLocation()
        self.result = nil
    }
}
