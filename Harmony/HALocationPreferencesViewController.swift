//
//  HALocationPreferencesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 09.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation


protocol HALocationPreferencesDelegate {
    func preferencesViewController(_ viewController: UIViewController, didSpecifyLocation location: CLLocation)
}

class HALocationPreferencesViewController: UITableViewController {
    public var delegate: HALocationPreferencesDelegate?
    
    public init() {
        super.init(style: .grouped)
        self.title = NSLocalizedString("Location", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - View lifecycle

extension HALocationPreferencesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.registerNibForCellReuse(named: "PreferenceCell")
    }
}


// MARK: - Table view delegate

private extension Selector {
    static let commitLocationCoordinates = #selector(HALocationCoordinatesPreferencesViewController.commitCoordinates)
}

extension HALocationPreferencesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let locationSearchPreferencesViewController = HALocationSearchPreferencesViewController()
            locationSearchPreferencesViewController.delegate = self.delegate
            self.navigationController?.pushViewController(locationSearchPreferencesViewController, animated: true)
            
        case (0, 1):
            let location = CLLocation()
            
            let locationCoordinatesPreferencesViewController = HALocationCoordinatesPreferencesViewController(showing: location)
            locationCoordinatesPreferencesViewController.shouldAllowResetting = false
            locationCoordinatesPreferencesViewController.shouldCommitOnViewDissapear = false
            locationCoordinatesPreferencesViewController.delegate = self.delegate
            
            locationCoordinatesPreferencesViewController.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: locationCoordinatesPreferencesViewController, action: .commitLocationCoordinates)
            self.navigationController?.pushViewController(locationCoordinatesPreferencesViewController, animated: true)
            
        case (1, 0):
            let settingsURL = URL(string: UIApplicationOpenSettingsURLString)!
            UIApplication.shared.openURL(settingsURL)
            
        default:
            break
        }
    }
}


// MARK: - Table view data source

extension HALocationPreferencesViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as! HANameValueCell
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            cell.nameLabel.text = NSLocalizedString("Search by name", comment: "")
        case (0, 1):
            cell.nameLabel.text = NSLocalizedString("Specify coordinates", comment: "")
        case (1, 0):
            cell.nameLabel.text = NSLocalizedString("Enable autodetection", comment: "")
        default:
            assertionFailure("Unexpected index path \(indexPath).")
            break
        }
        
        cell.valueLabel.text = nil
        return cell
    }
}


// MARK: - User defaults integration

private let HALocationPreferencesKey = "Location"
private let HALongitudePreferencesKey = "Longitude"
private let HALatitudePreferencesKey = "Latitude"

extension UserDefaults {
    var preferredLocation: CLLocation? {
        get {
            if let location = self.dictionary(forKey: HALocationPreferencesKey) as? [String: Double],
                let latitude = location[HALatitudePreferencesKey],
                let longitude = location[HALongitudePreferencesKey] {
                return CLLocation(latitude: latitude, longitude: longitude)
            }
            else {
                return nil
            }
        }
        
        set {
            if let location = newValue {
                let location: [String: Double] = [
                    HALongitudePreferencesKey: location.coordinate.longitude,
                    HALatitudePreferencesKey: location.coordinate.latitude
                ]
                
                self.set(location, forKey: HALocationPreferencesKey)
            }
            else {
                self.removeObject(forKey: HALocationPreferencesKey)
            }
        }
    }
}
