//
//  HALocationSearchPreferencesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 07.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation


class HALocationSearchPreferencesViewController: UITableViewController {
    fileprivate lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = NSLocalizedString("Search city, street or place", comment: "")
        
        return searchController
    }()
    
    fileprivate lazy var geocoder = CLGeocoder()
    fileprivate var searchResults: [CLPlacemark] = []
    
    public var delegate: HALocationPreferencesDelegate?
    
    public init() {
        super.init(style: .plain)
        self.title = NSLocalizedString("Search location", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


// MARK: - View lifecycle

extension HALocationSearchPreferencesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.definesPresentationContext = true
        self.tableView.tableHeaderView = self.searchController.searchBar
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.registerNibForCellReuse(named: "PreferenceCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // NOTE: search bar strangely is not becoming first responder without wrapping
        // `becomefirstResponder` into a dispatch asynchronous call
        DispatchQueue.main.async {
            self.searchController.searchBar.becomeFirstResponder()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchController.isActive = false
    }
}


// MARK: - Table view delegate

extension HALocationSearchPreferencesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let location = self.searchResults[indexPath.row].location {
            self.delegate?.preferencesViewController(self, didSpecifyLocation: location)
        }
    }
}


// MARK: - Table view data source

extension HALocationSearchPreferencesViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResults.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // todo: format address using CNPostalAddressFormatter
        let addressDictionary = self.searchResults[indexPath.row].addressDictionary
        let addressLines = addressDictionary?["FormattedAddressLines"] as? [String]
        let formattedAddress = addressLines?.joined(separator: ", ")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as! HANameValueCell
        cell.nameLabel.text = formattedAddress
        cell.valueLabel.text = nil
        cell.accessoryType = .none
        return cell
    }
}


// MARK: - Search results updater

extension HALocationSearchPreferencesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        self.geocoder.cancelGeocode()
        
        if let searchText = searchController.searchBar.text, searchText.isEmpty == false {
            self.geocoder.geocodeAddressString(searchText) { placemarks, error in
                if let placemarks = placemarks, error == nil {
                    self.searchResults = placemarks
                    self.tableView.reloadData()
                }
            }
        }
        else {
            self.searchResults = []
            self.tableView.reloadData()
        }
    }
}
