//
//  HAPointFormatter.swift
//  Harmony
//
//  Created by Сергей Цыба on 23.09.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import Foundation
import Ascension


// MARK: - Horizontal point formatting

class HAHorizontalPointFormatter {
    public var angleStyle: HAAngleFormatter.Style = .standard
    
    private func format(_ point: ASHorizontalPoint) -> (azimuth: String, altitude: String) {
        let formatter = HAAngleFormatter()
        formatter.style = self.angleStyle == .standard
            ? .decimalDegree
            : self.angleStyle
        
        let azimuth = formatter.string(from: point.azimuth)
        let altitude = formatter.string(from: point.altitude)
        
        return (azimuth, altitude)
    }
    
    /// Formats the specified horizontal point.
    public func string(from point: ASHorizontalPoint) -> String {
        let (azimuth, altitude) = self.format(point)
        return "Az:\(azimuth), a:\(altitude)"
    }
    
    /// Formats the spcified horizontal point as an attributed string.
    public func attributedString(from point: ASHorizontalPoint) -> NSAttributedString {
        let (azimuth, altitude) = self.format(point)
        
        let string = NSMutableAttributedString()
        string.append("Az:")
        string.append(azimuth, withAttributes: [
            "ordinateName": "azimuth",
            "localizedOrdinateName": NSLocalizedString("azimuth", comment: "")
            ])
        
        string.append(", a:")
        string.append(altitude, withAttributes: [
            "ordinateName": "altitude",
            "localizedOrdinateName": NSLocalizedString("altitude", comment: "")
            ])
        
        return string
    }
}


// MARK: - Equatorial points formatting

class HAEquatorialPointFormatter {
    public var angleStyle: HAAngleFormatter.Style = .standard
    
    private func format(_ point: ASEquatorialPoint) -> (rightAscension: String, declination: String) {
        let formatter = HAAngleFormatter()
        formatter.style = self.angleStyle == .standard
            ? .sexagesimalHour
            : self.angleStyle
        
        let rightAscension = formatter.string(from: point.rightAscension)
        
        formatter.style = self.angleStyle == .standard
            ? .sexagesimalDegree
            : self.angleStyle
        
        let declination = formatter.string(from: point.declination)
        
        return (rightAscension, declination)
    }
    
    /// Formats the specified equatorial point.
    public func string(from point: ASEquatorialPoint) -> String {
        let (rightAscension, declination) = self.format(point)
        return "α:\(rightAscension), δ:\(declination)"
    }
    
    /// Formats the specified equatorial point as an attributed string.
    public func attributedString(from point: ASEquatorialPoint) -> NSAttributedString {
        let (rightAscension, declination) = self.format(point)
        
        let string = NSMutableAttributedString()
        string.append("α:")
        string.append(rightAscension, withAttributes: [
            "ordinateName": "rightAscension",
            "localizedOrdinateName": NSLocalizedString("right ascension", comment: "")
            ])
        
        string.append(", δ:")
        string.append(declination, withAttributes: [
            "ordinateName": "declination",
            "localizedOrdinateName": NSLocalizedString("declination", comment: "")
            ])
        
        return string
    }
}

class HALocalEquatorialPointFormatter {
    public var angleStyle: HAAngleFormatter.Style = .standard
    
    private func format(_ point: HALocalEquatorialPoint) -> (rightAscension: String, declination: String) {
        let formatter = HAAngleFormatter()
        formatter.style = self.angleStyle == .standard
            ? .sexagesimalHour
            : self.angleStyle
        
        let hourAngle = formatter.string(from: point.hourAngle)
        
        formatter.style = self.angleStyle == .standard
            ? .sexagesimalDegree
            : self.angleStyle
        
        let declination = formatter.string(from: point.declination)
        
        return (hourAngle, declination)
    }
    
    /// Formats the specified equatorial point.
    public func string(from point: HALocalEquatorialPoint) -> String {
        let (hourAngle, declination) = self.format(point)
        return "h:\(hourAngle), δ:\(declination)"
    }
    
    /// Formats the specified equatorial point as an attributed string.
    public func attributedString(from point: HALocalEquatorialPoint) -> NSAttributedString {
        let (hourAngle, declination) = self.format(point)
        
        let string = NSMutableAttributedString()
        string.append("h:")
        string.append(hourAngle, withAttributes: [
            "ordinateName": "hourAngle",
            "localizedOrdinateName": NSLocalizedString("hour angle", comment: "")
            ])
        
        string.append(", δ:")
        string.append(declination, withAttributes: [
            "ordinateName": "declination",
            "localizedOrdinateName": NSLocalizedString("declination", comment: "")
            ])
        
        return string
    }
    
}


// MARK: - Geographic point formatting

class HAGeographicPointFormatter {
    public var angleStyle: HAAngleFormatter.Style = .standard
    
    private var angleFormatter: HAAngleFormatter {
        let formatter = HAAngleFormatter()
        formatter.style = self.angleStyle == .standard
            ? .sexagesimalDegree
            : self.angleStyle
        
        return formatter
    }
    
    private func format(longitude: ASAngle) -> String {
        let hemisphereMarker = longitude > 0.0
            ? NSLocalizedString("E", comment: "East hemisphere marker.")
            : NSLocalizedString("W", comment: "West hemisphere marker.")
        
        let longitude = fabs(longitude)
        let formattedLongitude = self.angleFormatter.string(from: longitude)
        return "\(formattedLongitude)\(hemisphereMarker)"
    }
    
    private func format(latitude: ASAngle) -> String {
        let hemisphereMarker = latitude > 0.0
            ? NSLocalizedString("N", comment: "Northern hemisphere marker.")
            : NSLocalizedString("S", comment: "Southern hemisphere marker.")
        
        let latitude = fabs(latitude)
        let formattedLatitude = self.angleFormatter.string(from: latitude)
        return "\(formattedLatitude)\(hemisphereMarker)"
    }
    
    /// Formats the specified geographic point.
    public func string(from point: ASGeographicPoint) -> String {
        let longitude = self.format(latitude: point.longitude)
        let latitude = self.format(latitude: point.latitude)
        
        return "\(longitude) \(latitude)"
    }
    
    /// Formats the specified geographic point as an attrbute string.
    public func attributedString(from point: ASGeographicPoint) -> NSAttributedString {
        let longitude = self.format(longitude: point.longitude)
        let latitude = self.format(latitude: point.latitude)
        
        let string = NSMutableAttributedString()
        string.append(longitude, withAttributes: [
            "ordinateName": "longitude",
            "localizedOrdinateName": NSLocalizedString("longitude", comment: "")
            ])
        
        string.append(" ")
        string.append(latitude, withAttributes: [
            "ordinateName": "latitude",
            "localizedOrdinateName": NSLocalizedString("latitude", comment: "")
            ])
        
        return string
    }
}


// MARK: - Convenience functionality

extension NSMutableAttributedString {
    func append(_ string: String, withAttributes attributes: [String: Any]? = nil) {
        let string = NSAttributedString(string: string, attributes: attributes)
        self.append(string)
    }
}
