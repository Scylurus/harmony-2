//
//  HAPreferencesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 03.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit
import CoreLocation


class HAPreferencesViewController: UITableViewController {
    public var shouldShowLocationPreferences = false {
        didSet {
            if self.isViewLoaded {
                let sections = IndexSet(integer: 2)
                
                switch self.tableView.numberOfSections {
                case 2 where self.shouldShowLocationPreferences == true:
                    self.tableView.insertSections(sections, with: .bottom)
                case 3 where self.shouldShowLocationPreferences == false:
                    self.tableView.deleteSections(sections, with: .bottom)
                default:
                    break
                }
            }
        }
    }
    
    public init() {
        super.init(style: .grouped)
        self.title = NSLocalizedString("Preferences", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - View lifecycle

extension HAPreferencesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.registerNibForCellReuse(named: "PreferenceCell")
    }
}


// MARK: - Table view delegate

extension HAPreferencesViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            let showBodiesPreferencesViewController = HAShowBodiesPreferencesViewController()
            self.navigationController?.pushViewController(showBodiesPreferencesViewController, animated: true)
            
        case (0, 1):
            let appearancePreferenceViewController = HAAppearancePreferenceViewController()
            self.navigationController?.pushViewController(appearancePreferenceViewController, animated: true)
            
        case (1, 0):
            let coordinateSystemPreferencesViewController = HACoordinateSystemPreferencesViewController()
            self.navigationController?.pushViewController(coordinateSystemPreferencesViewController, animated: true)
            
        case (1, 1):
            let angularMeasurePreferencesViewController = HAAngularMeasurePreferencesViewController()
            self.navigationController?.pushViewController(angularMeasurePreferencesViewController, animated: true)
            
        case (2, 0):
            let locationSearchPreferencesViewController = HALocationSearchPreferencesViewController()
            locationSearchPreferencesViewController.delegate = self
            self.navigationController?.pushViewController(locationSearchPreferencesViewController, animated: true)
            
        case (2, 1):
            let location = UserDefaults.standard.preferredLocation
                ?? CLLocation()
            
            let locationCoordinatesPreferencesViewController = HALocationCoordinatesPreferencesViewController(showing: location)
            locationCoordinatesPreferencesViewController.delegate = self
            locationCoordinatesPreferencesViewController.shouldCommitOnViewDissapear = true
            self.navigationController?.pushViewController(locationCoordinatesPreferencesViewController, animated: true)
            
        case (2, 2):
            let application = UIApplication.shared
            let settingsURL = URL(string: UIApplicationOpenSettingsURLString)!
            application.openURL(settingsURL)
            
        default:
            break
        }
    }
}


// MARK: - Table view data source

extension HAPreferencesViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.shouldShowLocationPreferences ? 3 : 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 2
        case 2:
            return 3
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return nil
        case 1:
            return NSLocalizedString("Formatting", comment: "")
        case 2:
            return NSLocalizedString("Location", comment: "")
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceCell") as! HANameValueCell
        
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            cell.nameLabel.text = NSLocalizedString("Show bodies", comment: "")
        case (0, 1):
            cell.nameLabel.text = NSLocalizedString("Appearance", comment: "")
        case (1, 0):
            cell.nameLabel.text = NSLocalizedString("Coordinate system", comment: "")
        case (1, 1):
            cell.nameLabel.text = NSLocalizedString("Angular measure", comment: "")
        case (2, 0):
            cell.nameLabel.text = NSLocalizedString("Search by name", comment: "")
        case (2, 1):
            cell.nameLabel.text = NSLocalizedString("Specify coordinates", comment: "")
        case (2, 2):
            cell.nameLabel.text = NSLocalizedString("Enable autodetection", comment: "")
        default:
            assertionFailure("Unexpected index path \(indexPath).")
            break
        }
        
        cell.valueLabel.text = nil
        return cell
    }
}


// MARK: - Location preferences delegate

extension HAPreferencesViewController: HALocationPreferencesDelegate {
    func preferencesViewController(_ viewController: UIViewController, didSpecifyLocation location: CLLocation) {
        UserDefaults.standard.preferredLocation = location
        
        // pop location search view controller when user taps search result
        if viewController is HALocationSearchPreferencesViewController {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
}


// MARK: - Table view cells

class HAValueCell: UITableViewCell {
    @IBOutlet var valueLabel: UILabel!
}

class HANameValueCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
}
