//
//  HAShowBodiesPreferencesViewController.swift
//  Harmony
//
//  Created by Сергей Цыба on 03.05.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit


class HAShowBodiesPreferencesViewController: UITableViewController {
    fileprivate let bodies: [[HACelestialBody]] = [
        [
            .sun,
            .moon
        ],
        [
            .mercury,
            .venus,
            .mars,
            .jupiter,
            .saturn,
            .uranus,
            .neptune
        ]
    ]
    
    fileprivate var remainingBodyIndexPath: IndexPath?
    
    public init() {
        super.init(style: .grouped)
        self.title = NSLocalizedString("Show bodies", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


// MARK: - Custom functionality

private extension HAShowBodiesPreferencesViewController {
    /// Returns index path of a cell of the specified body.
    private func cellIndexPath(of body: HACelestialBody) -> IndexPath? {
        for (section, bodySection) in self.bodies.enumerated() {
            if let row = bodySection.index(of: body) {
                return IndexPath(row: row, section: section)
            }
        }
        
        return nil
    }
    
    /// In case a single switch remains toggled on, reloads the cell with that switch to disable
    /// its interactivity. In case there are several switches toggled on, and one of them is
    /// non-interactable, reloads cell with that witch to restore its interactivity.
    ///
    /// Disabling interactivity on a single remaining on switch ensures that there is at least one
    /// body data to be shown.
    func accountForRemainingBody() {
        let shownBodies = UserDefaults.standard.shownBodies
        if shownBodies.count == 1 {
            // only a single switch remains toggled on, reload its cell to disable interaction on
            // the switch
            if let remainingBody = shownBodies.first,
                let indexPath = self.cellIndexPath(of: remainingBody) {
                self.remainingBodyIndexPath = indexPath
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
        else if let indexPath = self.remainingBodyIndexPath {
            // several switches are now toggled on, reload cell with previously disabled switch to
            // restore its iteractivity
            self.remainingBodyIndexPath = nil
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }

    /// Returns `true` if preferences indicate that the specified body is currently shown,
    /// `false` otherwise.
    func isShown(_ body: HACelestialBody) -> Bool {
        return UserDefaults.standard.shownBodies.contains(body)
    }
    
    /// Saves preference for whether the specified body is to be shown or not.
    func setShown(_ body: HACelestialBody, _ shown: Bool) {
        var shownBodies = UserDefaults.standard.shownBodies
        
        // check whether specified body data is already set to being shown
        if let index = shownBodies.index(of: body) {
            if shown == false {
                shownBodies.remove(at: index)
            }
        }
        else if shown {
            shownBodies.append(body)
        }
        
        // this ensures that shown body names are saved to user defaults in the order defined in
        // the bodies array
        UserDefaults.standard.shownBodies = self.bodies
            .flatMap() { $0 }
            .filter() { shownBodies.contains($0) }
    }
}


// MARK: - Target actions

private extension Selector {
    static let valueSwitchDidToggle = #selector(HAShowBodiesPreferencesViewController.valueSwitchDidToggle(_:))
}

private extension HAShowBodiesPreferencesViewController {
    @IBAction func valueSwitchDidToggle(_ sender: UISwitch!) {
        let point = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: point) {
            let body = self.bodies[indexPath.section][indexPath.row]
            self.setShown(body, sender.isOn)
            self.accountForRemainingBody()
        }
    }
}


// MARK: - View lifecycle

extension HAShowBodiesPreferencesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 55.0
        self.tableView.registerNibForCellReuse(named: "PreferenceSwitchCell")
        
        self.accountForRemainingBody()
    }
}


// MARK: - Table view delegate

extension HAShowBodiesPreferencesViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? HAPreferenceSwitchCell {
            cell.valueSwitch.addTarget(self, action: .valueSwitchDidToggle, for: .valueChanged)
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}


// MARK: - Table view data source

extension HAShowBodiesPreferencesViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.bodies.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bodies[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return NSLocalizedString("Luminaries", comment: "")
        case 1:
            return NSLocalizedString("Planets", comment: "")
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let body = self.bodies[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferenceSwitchCell") as! HAPreferenceSwitchCell
        cell.nameLabel.text = NSLocalizedString(body.name, comment: "")
        cell.valueSwitch.isOn = self.isShown(body)
        
        // disable interaction on the last remaining toggled on switch
        cell.valueSwitch.isEnabled = indexPath != self.remainingBodyIndexPath
        return cell
    }
}


// MARK: - Table view cells

class HAPreferenceSwitchCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var valueSwitch: UISwitch!
}


// MARK: - User defaults integration

private let shownBodiesKey = "ShowBodies"

extension UserDefaults {
    var shownBodies: [HACelestialBody] {
        get {
            var shownBodies: [HACelestialBody] = []
            if let shownBodyNames = self.array(forKey: shownBodiesKey) as? [String] {
                shownBodies = shownBodyNames.flatMap() {
                    switch $0.lowercased() {
                    case "sun":
                        return .sun
                    case "moon":
                        return .moon
                    case "mercury":
                        return .mercury
                    case "venus":
                        return .venus
                    case "mars":
                        return .mars
                    case "jupiter":
                        return .jupiter
                    case "saturn":
                        return .saturn
                    case "uranus":
                        return .uranus
                    case "neptune":
                        return .neptune
                    default:
                        return nil
                    }
                }
            }
            
            return shownBodies.isEmpty
                ? HACelestialBody.all
                : shownBodies
        }
        
        set {
            if newValue.isEmpty {
                self.removeObject(forKey: shownBodiesKey)
            }
            else {
                let shownBodyNames = newValue.map() {
                    return $0.name
                }
                
                self.set(shownBodyNames, forKey: shownBodiesKey)
            }
        }
    }
}


// MARK: - Celestial bodies

enum HACelestialBody {
    case sun
    case moon
    case mercury
    case venus
    case mars
    case jupiter
    case saturn
    case uranus
    case neptune
}

extension HACelestialBody {
    /// Name of the celestial body.
    var name: String {
        switch self {
        case .sun:
            return "Sun"
        case .moon:
            return "Moon"
        case .mercury:
            return "Mercury"
        case .venus:
            return "Venus"
        case .mars:
            return "Mars"
        case .jupiter:
            return "Jupiter"
        case .saturn:
            return "Saturn"
        case .uranus:
            return "Uranus"
        case .neptune:
            return "Neptune"
        }
    }
}

extension HACelestialBody {
    /// All supported celestial bodies in the default order.
    static let all: [HACelestialBody] = [
        .sun,
        .moon,
        .mercury,
        .venus,
        .mars,
        .jupiter,
        .saturn,
        .uranus,
        .neptune
    ]
}
