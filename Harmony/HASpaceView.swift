//
//  HASpaceView.swift
//  Harmony
//
//  Created by Сергей Цыба on 02.08.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import UIKit


class HASpaceView: UIScrollView {
    fileprivate let stellarDensity = 1.0
    fileprivate let stellarViewCount = 3
    
    fileprivate var gradientView: UIView!
    fileprivate var stellarViews: [UIView]!
    
    public init() {
        super.init(frame: CGRect.zero)
        self.setUpSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUpSubviews()
    }
}


// MARK: - Layout management

private let backgroundColors: [UIColor] = [
    UIColor(red: 0.0 / 255.0, green: 0.0 / 255.0, blue: 0.0 / 255, alpha: 1.0),
    UIColor(red: 11.0 / 255.0, green: 28.0 / 255.0, blue: 57.0 / 255, alpha: 1.0)
]

extension HASpaceView {
    fileprivate func setUpSubviews() {
        let gradientView = HAGradientView(colors: backgroundColors)
        gradientView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        gradientView.contentMode = .redraw
        self.addSubview(gradientView)
        
        let stellarDensity = self.stellarDensity / Double(self.stellarDensity)
        var stellarViews: [UIView] = []
        
        for _ in 0 ..< self.stellarViewCount {
            let stellarView = HAStellarView()
            stellarView.stellarDensity = stellarDensity
            stellarView.contentMode = .redraw
            stellarView.backgroundColor = .clear
            stellarViews.append(stellarView)
            
            self.addSubview(stellarView)
        }
        
        self.gradientView = gradientView
        self.stellarViews = stellarViews
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.gradientView.frame = self.bounds
        
        for (index, stellarView) in self.stellarViews.enumerated() {
            let scale = CGFloat(index + 1) / CGFloat(self.subviews.count)
            let origin = CGPoint(x: self.contentOffset.x - scale * self.contentOffset.x,
                                 y: self.contentOffset.y - scale * self.contentOffset.y)
            
            let size = CGSize(width: self.bounds.width + scale * (self.contentSize.width - self.bounds.width),
                              height: self.bounds.height + scale * (self.contentSize.height - self.bounds.height))
            
            stellarView.frame = CGRect(origin: origin, size: size)
        }
    }
}


// MARK: - Stellar view

/// Number of stars visible by a naked eye (magnitude < 6.5) in one hemisphere.
private let maximumStarCount = 9096 / 2

/// Number of stars drawn in a standard area rectangle with stellar density of 1.0.
/// Assumes standard area rectangle corresponds to a segment of 3° of a hemisphere.
private let standardStarCount = maximumStarCount / 120
private let standardArea = 320.0 * 568.0

private class HAStellarView: UIView {
    private let brightness: CGFloat = 0.75
    private let maximumDiameter: UInt32 = 3
    
    public var stellarDensity = 1.0 {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(gray: self.brightness, alpha: 1.0)
            
            let areaRatio = Double(rect.width * rect.height) / standardArea
            let starCount = Int(self.stellarDensity * areaRatio) * standardStarCount
            
            for _ in 0 ..< starCount {
                let diameter = arc4random_uniform(self.maximumDiameter)
                let size = CGSize(width: CGFloat(diameter), height: CGFloat(diameter))
                
                let starRect = CGRect(origin: rect.randomPoint, size: size)
                context.fillEllipse(in: starRect)
            }
        }
    }
}


// MARK: - Gradient view

private class HAGradientView: UIView {
    init(colors: [UIColor]) {
        super.init(frame: CGRect.zero)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = backgroundColors.map() {
            return $0.cgColor
        }
        
        self.layer.addSublayer(gradientLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        if let sublayers = self.layer.sublayers {
            for sublayer in sublayers {
                sublayer.frame = self.bounds
            }
        }
        
        CATransaction.commit()
    }
}


// MARK: - Convenience functionality

private extension CGRect {
    var randomPoint: CGPoint {
        let x = arc4random_uniform(UInt32(self.width))
        let y = arc4random_uniform(UInt32(self.height))
        
        return CGPoint(x: CGFloat(x), y: CGFloat(y))
    }
}
