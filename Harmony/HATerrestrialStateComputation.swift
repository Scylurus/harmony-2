//
//  HAApparentStateComputation.swift
//  Harmony
//
//  Created by Сергей Цыба on 05.10.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import Foundation
import Ascension


class HATerrestrialStateComputation: HAAsynchronousOperation {
    public let object: ASCelestialObject
    public let observation: ASGeoObservation
    public private(set) var result: ASTerrestrialState?
    
    public init(of object: ASCelestialObject, at observation: ASGeoObservation) {
        self.object = object
        self.observation = observation
    }
    
    override func start() {
        guard self.isCancelled == false else {
            self.state = .finished
            return
        }
        
        self.state = .executing
        self.result = observation.apparentState(of: object)
        self.state = .finished
    }
}
