//
//  HarmonyTests.swift
//  HarmonyTests
//
//  Created by Сергей Цыба on 28.04.16.
//  Copyright © 2016 Tsyba Software Co. All rights reserved.
//

import XCTest
@testable import Harmony

class HAAngleFormatterTests: XCTestCase {
    func testAngleFormatter() {
        let formatter = HAAngleFormatter()
        let value = 1.4027853
        var formattedValue = ""
        
        formatter.style = .radian
        formattedValue = formatter.string(from: value)
        XCTAssertEqual("1.402785", formattedValue)
        
        formatter.style = .decimalDegree
        formattedValue = formatter.string(from: value)
        XCTAssertEqual("80°.373677", formattedValue)
        
        formatter.style = .sexagesimalDegree
        formattedValue = formatter.string(from: value)
        XCTAssertEqual("80°22′25″", formattedValue)
        
        formatter.style = .sexagesimalHour
        formattedValue = formatter.string(from: value)
        XCTAssertEqual("5ʰ21ᵐ30ˢ", formattedValue)
    }
}
